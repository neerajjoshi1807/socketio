
# Sockets.io

Socket.IO is a JavaScript library for real-time web applications. It enables real-time, bi-directional communication between web clients and servers. It has two parts: a client-side library that runs in the browser, and a server-side library for node.js.

In the following image, the working of sockets is visualized. 

At the server-side, the **`data-source`** is a module which accepts data from any external source.
**`stat`** is the data that is to be sent to the client. Socket.io sends the data from the server to the client.

At the client-side, the socket.io accepts the data(`stat`) from the server and displays it in the browser.

![alt text](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Nodejs+/socket.png)


The traditional web applications like LAMP (PHP) involves polling the server for changes. Polling is a usual term programmer used when they refer to a system that constantly asks a server for updates. For example, the web-based chat system does not have a method of knowing if there is a new message on the server. Thus the server needs to keep track of time, and it makes the whole system very slow and inefficient.

Sockets have been the solution around which most real-time systems are made, providing a **bi-directional communication channel** between a client and a server. This means that the server can push messages to clients and vice versa. But what creates a difference using `socket.io` is that whenever an event occurs, the server will automatically be notified and it will respond accordingly without polling. 

