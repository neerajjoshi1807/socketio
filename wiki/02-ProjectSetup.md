# Understanding socket through a project

We will understand the use of sockets by developing a chatbox-like application. We will be using the _express_ framework to create a connection to the server that Socket.IO will work with. We have already learned the _express_ module in detail in previous courses.


## Basic Socket Project-(Part1)

First, we will create a view for our application. Since we are making a chatbox-like application, we will be having 
- a chat-box which will contain all the chats,
- one input box which will be used to send the chat,
- also, we will have an option to change the username.

We will create an `ejs` file for views.

Create a file named **`index.ejs`** using the following command:

```js
touch index.ejs
```
Consider the following code in the `index.ejs` file:

```html


<!DOCTYPE html>
<!------------------------html and css---------------------->
<html>
  <head>
    <meta http-equiv="Content-Type" const="text/html;charset=UTF-8" />
    <link href="http://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="style.css" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <title>Simple Chat App</title>
  </head>

  <body>
    <header>
      <h1>Super Chat</h1>
    </header>

    <section>
        <div id="change_username">
          	<input id="username" type="text" />
          	<button id="send_username" type="button">Change username</button>
        </div>
    </section>

    <section id="chatroom">
        <section id="feedback"></section>
    </section>

    

    <section id="input_zone"> 
        <input id="message" class="vertical-align" type="text" />
        <button id="send_message" class="vertical-align" type="button">Send</button>
    </section>
<!-------------Client Side socket connection-------------->
    <script src="/socket.io/socket.io.js"></script>     <!--Line1-->
    
    <script>
      var socket = io.connect();        //Line2
    </script>

  </body>
</html>
```
All the above code is HTML and CSS code which we will learn in the Frontend course. 

The main lines to be considered are lines 1 and 2.

**For socket connection at the client-side, it is important to include the socket.io script as done in _Line1_ and also to create a connection object as created in _Line2_**. These 2 lines will be copied as it is whenever we need to connect the socket framework with our client.